import app from './App';
import { port } from './envSettings';

app.listen(port, () => {
  console.log(`server is listening on ${port}`);
});
