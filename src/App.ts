import * as express from 'express';
import * as bodyParser from 'body-parser';
import { createSchema, Type, typedModel } from 'ts-mongoose';
import * as cors from 'cors';

import apiRouter from './routes/api';

//DB
import db, { initial } from './database/mongoConnection';
import { port } from './envSettings';
import { checkDuplicateUsernameOrEmail } from './middlewares/verifySignUp';

class App {
  public express;
  // public savedGame: Array<GameSchemaType>;

  constructor() {
    this.express = express();

    this.mountRoutes();
  }

  private mountRoutes(): void {
    const rootRouter = express.Router();
    const getRouter = express.Router();
    const getNotesRoute = express.Router();
    //this.dbConnection();

    getRouter.get('/api/', this.getRouterHandler);
    rootRouter.get('/', this.rootRouterHandler);
    //getNotesRoute.get('/api/notes', this.getNotesHandler);

    this.express.use([this.setHeaders, getRouter, getNotesRoute, rootRouter]);
  }

  private setHeaders(req, res, next) {
    res.set({
      'Access-Control-Allow-Origin': `http://localhost:${port}`,
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
    });
    next();
  }

  private getRouterHandler(req, res, next) {
    res.send('API Work');
    next();
  }

  private rootRouterHandler(req, res, next) {
    res.send('index');
    next();
  }

  // private getNotesHandler(req, res, next) {
  //   const noteModel = typedModel('note', NoteSchema);
  //   noteModel.find({}, (err, docs: Array<NoteSchemaType>) => {
  //     err ? res.send({ err: err }) : res.send({ note: docs });
  //   });
  //   next();
  // }
  //
  // private dbConnection() {
  //   db.once('open', async () => {
  //     const Title = typedModel('Title', GameSchema);
  //     // const newGame = new Title({ title: 'Awakening' });
  //     Title.find({ title: 'Awakening' }, (err, docs: Array<GameSchemaType>) => {
  //       this.savedGame = docs;
  //     });
  //   });
  // }
}

const app = express();

const corsOptions = {
  origin: 'http://localhost:3010',
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

app.use('/api', apiRouter);

db.once('open', async () => {
  initial();
});

export default app;
