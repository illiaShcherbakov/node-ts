import { typedModel } from 'ts-mongoose';
import { RoleSchema } from '../schemas/RoleSchema';

export const RoleModel = typedModel('Role', RoleSchema);
