import { typedModel } from 'ts-mongoose';
import { UserSchema } from '../schemas/UserSchema';

export const UserModel = typedModel('User', UserSchema);
