import * as mongoose from 'mongoose';
import { RoleModel } from './models/RoleModel';

export function initial() {
  RoleModel.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new RoleModel({
        name: 'user',
      }).save((err) => {
        if (err) {
          console.log('error', err);
        }

        console.log("added 'user' to roles collection");
      });

      new RoleModel({
        name: 'moderator',
      }).save((err) => {
        if (err) {
          console.log('error', err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new RoleModel({
        name: 'admin',
      }).save((err) => {
        if (err) {
          console.log('error', err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}

const uri = 'mongodb+srv://admin:admin@fecluster-bzooj.azure.mongodb.net/node-learn?retryWrites=true&w=majority';
mongoose
  .connect(uri, { useNewUrlParser: true })
  .then(() => {
    console.log('Successfully connect to MongoDB.');
    //initial();
  })
  .catch((err) => {
    console.error('Connection error', err);
    process.exit();
  });

export default mongoose.connection;
