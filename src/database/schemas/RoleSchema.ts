import { createSchema, Type, ExtractDoc, ExtractProps } from 'ts-mongoose';

export const RoleSchema = createSchema({
  name: Type.string({ required: true }),
});

export type RoleSchemaDoc = ExtractDoc<typeof RoleSchema>;
export type RoleProps = ExtractProps<typeof RoleSchema>;
