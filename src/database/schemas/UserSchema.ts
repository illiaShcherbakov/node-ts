import { createSchema, Type, ExtractDoc, ExtractProps } from 'ts-mongoose';
import { RoleSchema } from './RoleSchema';

export const UserSchema = createSchema({
  username: Type.string({ required: true }),
  email: Type.string({ required: true }),
  password: Type.string({ required: true }),
  roles: Type.array().of(Type.ref(Type.objectId()).to('Role', RoleSchema)),
});

export type UserSchemaDoc = ExtractDoc<typeof UserSchema>;
export type UserProps = ExtractProps<typeof UserSchema>;
