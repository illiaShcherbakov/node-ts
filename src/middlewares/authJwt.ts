import * as jwt from 'jsonwebtoken';
import { secret } from '../config/authConfig';
import { UserModel } from '../database/models/UserModel';
import { UserSchemaDoc } from '../database/schemas/UserSchema';
import { RoleModel } from '../database/models/RoleModel';
import { RoleSchemaDoc } from '../database/schemas/RoleSchema';

export async function verifyToken(req, res, next) {
  let token = req.headers['x-access-token'];

  if (!token) {
    return res.status(401).send({ message: 'No token provided!' });
  }

  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }
    req.userId = decoded.id;
    next();
  });
}

export async function isAdmin(req, res, next) {
  try {
    const user: UserSchemaDoc = await UserModel.findById(req.userId).exec();
    const roles: Array<RoleSchemaDoc> = await RoleModel.find({ _id: { $in: user.roles } }).exec();
    const hasAdmin = roles.some((r) => r.name === 'admin');

    if (hasAdmin) return next();

    return res.status(403).send({ message: 'Require Admin Role!' });
  } catch (err) {
    return res.status(500).send({ message: err });
  }
}

export async function isModerator(req, res, next) {
  try {
    const user = await UserModel.findById(req.userId).exec();
    const roles = await RoleModel.find({
      _id: { $in: user.roles },
    });

    if (roles.some((role) => role.name === 'moderator')) return next();
    return res.status(403).send({ message: 'Require Moderator Role!' });
  } catch (err) {
    return res.status(500).send({ message: err });
  }
}
