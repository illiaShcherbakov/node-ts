import { UserModel } from '../database/models/UserModel';
import { ROLES } from '../database/constants';

export async function checkDuplicateUsernameOrEmail(req, res, next) {
  try {
    //Username
    const userByUsername = await UserModel.findOne({ username: req.body.username }).exec();
    if (userByUsername) return res.status(400).send({ message: 'Failed! Username is already in use!' });

    //Email
    const userByEmail = await UserModel.findOne({ email: req.body.email }).exec();
    if (userByEmail) return res.status(400).send({ message: 'Failed! Email is already in use!' });
  } catch (err) {
    res.status(500).send({ message: err });
    return;
  }

  next();
}

export async function checkRolesExisted(req, res, next) {
  const roles = req.body.roles;
  if (!roles.length)
    return res.status(400).send({
      message: `Failed! Must specify a role`,
    });

  roles.forEach((role) => {
    if (!ROLES.includes(role))
      return res.status(400).send({
        message: `Failed! Role ${role} does not exist!`,
      });
  });

  next();
}
