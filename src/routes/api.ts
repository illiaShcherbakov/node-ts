import * as express from 'express';
import * as api from '../controllers/api';
import { checkDuplicateUsernameOrEmail, checkRolesExisted } from '../middlewares/verifySignUp';
import app from '../App';
import { signin, signup } from '../controllers/auth';
import { adminBoard, allAccess, moderatorBoard, userBoard } from '../controllers/user';
import { isAdmin, isModerator, verifyToken } from '../middlewares/authJwt';

const apiRouter = express.Router();

//apiRouter.use(checkDuplicateUsernameOrEmail);
apiRouter.use((req, res, next) => {
  res.header('Access-Control-Allow-Headers', 'x-access-token, Origin, Content-Type, Accept');
  next();
});
apiRouter.get('/', api.index);

//Authentication
apiRouter.post('/auth/signup', [checkDuplicateUsernameOrEmail, checkRolesExisted], signup);
apiRouter.post('/auth/signin', signin);

//Authorization
apiRouter.get('/test/all', allAccess);
apiRouter.get('/test/user', [verifyToken], userBoard);
apiRouter.get('/test/mod', [verifyToken, isModerator], moderatorBoard);
apiRouter.get('/test/admin', [verifyToken, isAdmin], adminBoard);

export default apiRouter;
