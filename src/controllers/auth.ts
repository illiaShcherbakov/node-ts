import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { UserModel } from '../database/models/UserModel';
import { RoleModel } from '../database/models/RoleModel';
import { secret } from '../config/authConfig';
import { RoleSchemaDoc } from '../database/schemas/RoleSchema';

export async function signup(req, res) {
  try {
    const user = new UserModel({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
    });
    const newUser = await user.save();

    if (req.body.roles) {
      const roles = await RoleModel.find({
        name: { $in: req.body.roles },
      }).exec();

      newUser.roles = roles.map((role) => role._id);
      await newUser.save();
      return res.send({ message: 'User was registered successfully!' });
    }

    const role = await RoleModel.findOne({ name: 'user' }).exec();
    newUser.roles = [role._id];
    await newUser.save();
    return res.send({ message: 'User was registered successfully!' });
  } catch (err) {
    return res.status(500).send({ message: err });
  }
}

export async function signin(req, res) {
  try {
    const user = await UserModel.findOne({ username: req.body.username }).populate('roles', '-__v').exec();

    if (!user) return res.status(404).send({ message: 'User Not found.' });

    const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

    if (!passwordIsValid) return res.status(401).send({ accessToken: null, message: 'Invalid Password!' });

    const token = jwt.sign({ id: user.id }, secret, {
      expiresIn: 86400, // 24 hours
    });

    const authorities = (user.roles as RoleSchemaDoc[]).map((role) => `ROLE_${role.name.toUpperCase()}`);

    return res.status(200).send({
      id: user._id,
      username: user.username,
      email: user.email,
      roles: authorities,
      accessToken: token,
    });
  } catch (err) {
    return res.status(500).send({ message: err });
  }
}
